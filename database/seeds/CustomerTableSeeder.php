<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            ['Name' => 'Tín Lê',
            'PhoneNumber' => '0969296396',
            'CurrentOrder_OrderId' => NULL],
            ['Name' => 'Ngọc Dương',
                'PhoneNumber' => '01694569874',
                'CurrentOrder_OrderId' => NULL],
            ['Name' => 'Lý Hồ',
                'PhoneNumber' => '0937212112',
                'CurrentOrder_OrderId' => NULL],
            ['Name' => 'Tự Do',
                'PhoneNumber' => '01632939648',
                'CurrentOrder_OrderId' => NULL],
        ];
        DB::table('customers')->delete();
        foreach($customers as $customer){
            \App\Models\Customer::create($customer);
        }
    }
}
