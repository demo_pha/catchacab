<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaxiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxis = [
            ['MaxPassengers' => 6,
            'TaxiName' => 'Mai Linh',
            'Latitude' => 16.071682 ,
            'Longitude' =>  108.219503,
            'Status' =>  0,
            'LastUpdate' =>  NULL,
            'CurrentZone_ZoneId' => 4,
            'QueryEntryTime' => NULL,
            'CurrentOrder_OrderId' => NULL],

            ['MaxPassengers' => 6,
            'TaxiName' => 'Hàng Không',
            'Latitude' => 16.077909,
            'Longitude' => 108.212765 ,
            'Status' =>  1,
            'LastUpdate' =>  NULL,
            'CurrentZone_ZoneId' => 4,
            'QueryEntryTime' => NULL,
            'CurrentOrder_OrderId' => NULL],

            ['MaxPassengers' => 6,
            'TaxiName' => 'Tiên Sa',
            'Latitude' =>  16.066651,
            'Longitude' =>  108.223623,
            'Status' =>  0,
            'LastUpdate' =>  NULL,
            'CurrentZone_ZoneId' => 4,
            'QueryEntryTime' => NULL,
            'CurrentOrder_OrderId' => NULL],
        ];

        DB::table('taxis')->delete();
        foreach($taxis as $taxi){
            \App\Models\Taxi::create($taxi);
        }

    }
}
