<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('customers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('Name');
            $table->string('PhoneNumber')->primary();
            $table->integer('CurrentOrder_OrderId')->unsigned()->nullable();

        });
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('OrderId');
            $table->integer('PassengersNumber');
            $table->float('Src_Latitude',10,7);
            $table->float('Src_Longitude',10,7);
            $table->float('Des_Latitude',10,7);
            $table->float('Des_Longitude',10,7);
            $table->time('OrderTime');
            $table->time('DepartureTime');
            $table->integer('Status');
            $table->string('CustomerPhoneNumber')->index();
            $table->integer('TaxiId')->unsigned();
            $table->integer('SourceZoneId')->unsigned();
            $table->string('SourceAddress');
            $table->string('DestinationAddress');
        });

        Schema::create('taxis', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('TaxiId');
            $table->integer('MaxPassengers');
            $table->string('TaxiName');
            $table->float('Latitude',10,7);
            $table->float('Longitude',10,7);
            $table->integer('Status');    // 0 : free     1 : busy    2 : off duty
            $table->time('LastUpdate')->nullable();
            $table->integer('CurrentZone_ZoneId')->unsigned();
            $table->time('QueryEntryTime')->nullable();
            $table->integer('CurrentOrder_OrderId')->unsigned()->nullable();

        });

        Schema::create('zones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ZoneId');
            $table->string('Name');

        });


        Schema::table('customers', function ($table){
            $table->foreign('CurrentOrder_OrderId')->references('OrderId')->on('orders')->onUpdate('cascade');

        });
        Schema::table('orders', function($table){
            $table->foreign('CustomerPhoneNumber')->references('PhoneNumber')->on('customers')->onUpdate('cascade');
            $table->foreign('SourceZoneId')->references('ZoneId')->on('zones')->onUpdate('cascade');
            $table->foreign('TaxiId')->references('TaxiId')->on('taxis')->onUpdate('cascade');
        });

        Schema::table('taxis', function($table){
            $table->foreign('CurrentOrder_OrderId')->references('OrderId')->on('orders')->onUpdate('cascade');
            $table->foreign('CurrentZone_ZoneId')->references('ZoneId')->on('zones')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
        Schema::drop('orders');
        Schema::drop('zones');
        Schema::drop('taxis');
    }

}
