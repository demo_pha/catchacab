<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taxi extends Model
{
    protected $table = 'taxis';
    protected $primaryKey = 'TaxiId';
    public $timestamps = false;
}
