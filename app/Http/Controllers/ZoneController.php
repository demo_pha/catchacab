<?php

namespace App\Http\Controllers;

use App\Business\ZoneBusiness;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ZoneController extends Controller
{
    protected $zoneBusiness;
    public function __construct(ZoneBusiness $zoneBusiness){
        $this->zoneBusiness = $zoneBusiness;
    }

    public function index(){
        $zones = $this->zoneBusiness->getAllZones();
        if($zones){
            return response()->json([
                'zones' => $zones,
                'message'   => 'OK'
            ]);
        }
        return response()->json([
            'message' => 'No entry'
        ]);
    }

    public function show($zoneId){
        $zone = $this->zoneBusiness->getZone($zoneId);
        if($zone)   return response()->json([
            'zone'  => $zone,
            'message'   => 'OK'
        ]);
        return response()->json([
            'message'   => 'Zone does not exists'
        ]);
    }
}
