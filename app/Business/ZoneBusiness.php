<?php
namespace App\Business;
use App\Models\Zone;

/**
 * Created by PhpStorm.
 * User: PHA
 * Date: 04/01/2017
 * Time: 21:46
 */
class ZoneBusiness
{
    public function getAllZones(){
        return Zone::all();
    }

    public function getZone($zoneId){
        $zone = Zone::where('ZoneId',$zoneId)->first();
        return ($zone != null)?$zone:null;
    }

}